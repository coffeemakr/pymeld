#
# Extra tests, for features that aren't tested by the (visible) docstrings:
#

import unittest
from PyMeld import *

class TestPyMeld(unittest.TestCase):
	def test_entities_and_charrefs(self):
		page = Meld('''<html><body>&bull; This "and&#160;that"...
<span id="s" title="&quot;Quoted&quot; & Not">x</span></body></html>''')
		self.assertEqual(page.s.title, '"Quoted" & Not')
		page.s.title = page.s.title     # Accept liberally, produce strictly.
		self.assertEqual(str(page), """<html><body>&bull; This "and&#160;that"...
<span id="s" title="&quot;Quoted&quot; &amp; Not">x</span></body></html>""")
		page.s.title = page.s.title + " <>"
		self.assertEqual(str(page.s.title), '"Quoted" & Not <>')
		page_s = '<span id="s" title="&quot;Quoted&quot; &amp; Not &lt;&gt;">x</span>'
		self.assertEqual(str(page.s), page_s)

	def test_assigning_to_content(self):
		page = Meld('''<html><span id="s">Old</span></html>''')
		page.s._content = "New"
		self.assertEqual(str(page), '<html><span id="s">New</span></html>')
		page._content = "All new"
		self.assertEqual(page._content, "All new")
		self.assertEqual(str(page), '<html>All new</html>')

	def test_deleting_content(self):
		page = Meld('''<html><span id="s">Old</span></html>''')
		del page.s._content
		self.assertEqual(str(page), '<html><span id="s"></span></html>')

	def test_constructing_from_an_unknown_type(self):
		self.assertRaises(TypeError, Meld, (1,))

	def test_accessing_a_non_existent_attribute(self):
		page = Meld('<html><body id="body"></body></html>')
		self.assertRaises(AttributeError, getattr, page, 'spam')
		self.assertRaises(AttributeError, delattr, page, 'spam')
		self.assertRaises(AttributeError, getattr, page.body, 'spam')    # For non-container Melds
		self.assertRaises(AttributeError, delattr, page.body, 'spam')      # For non-container Melds

	def test_add_new_things(self):
		page = Meld('''<html><textarea id="empty"></textarea></html>''')
		page.empty = "Not any more"
		page.empty.cols = 60
		self.assertEqual(str(page), '<html><textarea cols="60" id="empty">Not any more</textarea></html>')

	def test_readonly(self):
		msg = "You can't modify this read-only Meld object"
		page = Meld('''<html><span id='no'>No!</span></html>''', readonly=True)
		self.assertRaises(ReadOnlyError, setattr, page, 'no', 'Yes?')
		self.assertRaises(ReadOnlyError, page.__iadd__, "More?")
		self.assertRaises(ReadOnlyError, delattr, page, 'no')

	def test_copy_from_one_to_another(self):
		a = Meld('<html><span id="one">One</span></html>')
		b = Meld('<html><span id="two">Two</span></html>')
		a.one = b.two
		self.assertEqual(str(a), '<html><span id="one"><span id="two">Two</span></span></html>')
		b.two = "New"
	  	# Checking for side-effects
		self.assertEqual(str(a), '<html><span id="one"><span id="two">Two</span></span></html>')

	def test_mixed_type_add_radd_and_iadd(self):
		a = Meld('<html><span id="one">1</span></html>')
		self.assertEqual(a.one + "x", '<span id="one">1</span>x')
		self.assertEqual("x" + a.one, 'x<span id="one">1</span>')
		a.one += "y<z></z>"
		self.assertEqual(str(a), '<html><span id="one">1y<z></z></span></html>')
	
	def test_access_top_level_element(self):
		d = Meld("<x id='x'>spam</x>")
		self.assertEqual(str(d.x), "<x id='x'>spam</x>")
	
	def test_access_nested_element_with_same_name(self):
		d = Meld("<x id='x'><x id='x'>spam</x></x>")
		self.assertEqual(str(d.x.x), "<x id='x'>spam</x>")
		d = Meld("<outer><x id='x'><x id='x'>spam</x></x></outer>")
		self.assertEqual(str(d.x.x), "<x id='x'>spam</x>")

	# This is just a smoke-test; proper Unicode support is untested, though
	# the code does attempt to be unicode-friendly.
	def test_unicode(self):
		u = Meld(u'<html><span id="one">One</span></html>')
		a = Meld('<html><span id="two">Two</span></html>')
		u.one = a.two
		self.assertEqual(unicode(u), u'<html><span id="one"><span id="two">Two</span></span></html>')
		a.two = Meld(u'<x a="Unicode Value"/>')
		self.assertEqual(str(a), '<html><span id="two"><x a="Unicode Value"/></span></html>')

	def test_more_unicode(self):
		html = u'<html><div id="about">I \u2665 HTML</div></html>'
		meld = Meld(html)
		self.assertEqual(unicode(meld), u'<html><div id="about">I \u2665 HTML</div></html>')
		self.assertEqual(str(meld), '<html><div id="about">I &#9829; HTML</div></html>')
		self.assertEqual(str(getattr(meld, u'about')), '<div id="about">I &#9829; HTML</div>')
		self.assertEqual(str(getattr(meld, 'about')), '<div id="about">I &#9829; HTML</div>')

	def test_private_attributes(self):
		page = Meld('<html>x</html>')
		page._private = "Spam"
		self.assertEqual(repr(page._private), "'Spam'")
		self.assertEqual(str(page), '<html>x</html>')
		del page._private
		self.assertRaises(AttributeError, getattr, page, '_private')
		self.assertRaises(AttributeError, delattr, page, '_private')
		self.assertEqual(str(page), '<html>x</html>')

	def test_no_markup(self):
		page = Meld("Hello world")
		self.assertRaises(ValueError, getattr, page, 'spam')
		# self.assertEqual(str(why.exception), "This isn't any form of markup I recognize")

	def test_nesting(self):
		page = Meld('''<html>
<span id="all">Hello
    <span id="who">World
        <span id='extra'>and friends</span>
    </span>!
    <span id="goodbye">Goodbye</span>
</span>
</html>''')
		self.assertEqual(str(page.all), '''<span id="all">Hello
    <span id="who">World
        <span id='extra'>and friends</span>
    </span>!
    <span id="goodbye">Goodbye</span>
</span>''')
		self.assertEqual(str(page.extra), "<span id='extra'>and friends</span>")


	def test_re_bug(self):
		page = Meld("<A a='a'><B b='b'><C c='c' id='x'></C></B></A>")
		self.assertEqual(str(page.x), "<C c='c' id='x'></C>")

	def test_underscores(self):
		html = '<html><div id="header-box" dash-attr="_">xxx</div></html>'
		meld = Meld(html, replaceUnderscoreWithDash=True)
		self.assertEqual(str(meld % {'header_box': 'Mod'}), '<html><div id="header-box" dash-attr="_">Mod</div></html>')
		self.assertEqual(str(meld.toFormatString(useDict=True)), '<html><div id="header-box" dash-attr="_">%(header_box)s</div></html>')
		meld.header_box = 'yyy'
		meld.header_box.dash_attr = '___'
		self.assertEqual(str(meld), '<html><div id="header-box" dash-attr="___">yyy</div></html>')

	def test_doctype(self):
		html = '<!DOCTYPE html PUBLIC "abc" "xyz">\n<html><x id="y">z</x></html>'
		meld = Meld(html)
		meld.a = 'a'
		meld.y = 'b'
		self.assertEqual(str(meld), '<!DOCTYPE html PUBLIC "abc" "xyz">\n<html a="a"><x id="y">b</x></html>')

	def test_unicode_doctype(self):
		html = '<!DOCTYPE html PUBLIC "abc" "xyz">\n<html><x id="y">z</x></html>'
		meld = Meld(html)
		meld.a = 'a'
		meld.y = 'b'
		self.assertEqual(str(unicode(meld)), '<!DOCTYPE html PUBLIC "abc" "xyz">\n<html a="a"><x id="y">b</x></html>')

	def test_doctype_2(self):
		html = '<!DOCTYPE html>\n<html><x id="y">z</x></html>'
		meld = Meld(html)
		meld._content = '<a href="#">Link</a>'
		self.assertEqual(str(unicode(meld)), '''<!DOCTYPE html>
<html><a href="#">Link</a></html>''')
		meld = meld.clone()
		self.assertEqual(str(meld), '''<!DOCTYPE html>
<html><a href="#">Link</a></html>''')
		self.assertEqual(str(meld._content), '<a href="#">Link</a>')


	def test_eichin_bug(self):
		page = Meld('''<table><tr><td
id="Instance_1_0">label</td><td id="Instance_1_1"></td><td
id="Instance_1_2"></td><td id="Instance_1_3"> </td><td class="running"
id="Instance_1_4">Running</td></tr>
</table>''')
		self.assertEqual(str(page.Instance_1_4), '''<td class="running"
id="Instance_1_4">Running</td>''')

	def test_immediat_closing(self):
		page = Meld('<html><span id="x"/></html>')
		self.assertEqual(str(page.x), '''<span id="x"/>''')

	def test_openIDTagRE_1(self):
		m = re.search(openIDTagRE % 'x', "<A a='b'><B c='d' id='x'>")
		self.assertEqual(m.group('tag'), 'B')
		self.assertEqual(m.group('id'), 'x')
		self.assertEqual(m.span(), (9, 25))


	def test_openIDTagRE_2(self):
		m = re.search(openIDTagRE % 'xYz', '''<A a='b'><XYB c="d" id="xyz">''')
		self.assertEqual(m.group('tag'), 'XYB')
		self.assertEqual(m.group('id'), 'xyz')
		page = Meld('''<A a='b'><XYb c="d" id="xyz">hallo</XyB></A>''')
		self.assertEqual(str(page.xyz), '<XYb c="d" id="xyz">hallo</XyB>')
	
	def test_boolean_attributes(self):
		'''
		See http://www.w3.org/html/wg/drafts/html/master/infrastructure.html
		for more information.
		'''
		m = Meld('<html><input id="i" type="checkbox" disabled/></html>')
		self.assertEqual(str(m), '<html><input id="i" type="checkbox" disabled/></html>')
		i = m.i
		self.assertEqual(str(i), '<input id="i" type="checkbox" disabled/>')
		i.disabled = False
		self.assertEqual(str(i), '<input id="i" type="checkbox"/>')
		m = Meld('<html><input id="i" type="checkbox" disabled/></html>')
		m.i.disabled = "disabled"
		self.assertEqual(str(m.i), '<input id="i" type="checkbox" disabled="disabled"/>')
		del m.i.disabled
		m.i.disabled = True
		self.assertEqual(str(m.i), '<input disabled id="i" type="checkbox"/>')

if __name__ == '__main__':
	unittest.main()